
exports.up = function(knex, Promise) {
  return knex.schema.createTable('monkeys', (t) => {
    t.increments();
    t.string('name');
    t.integer('size');
    t.timestamps(true, true);
  });  
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('monkeys');  
};
