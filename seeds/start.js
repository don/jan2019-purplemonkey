
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('monkeys').del()
    .then(function () {
      // Inserts seed entries
      return knex('monkeys').insert([
        {name: 'Bob', size: 8},
        {name: 'Roberta', size: 7},
        {name: 'Bob Jr', size: 5}
      ]);
    });
};
