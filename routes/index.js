const express = require('express');
const router = express.Router();
const env = process.env.NODE_ENV || 'development';
const knexConfig = require('../knexfile');
const knex = require('knex')(knexConfig[env]);


/* GET home page. */
router.get('/', (req, res, next) => {
  knex('monkeys').then(monkeys => {
    res.render('index', { title: 'Purple Monkeys', monkeys });
  });
});

module.exports = router;
